// WanZhangArchive.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <thread>
#include <string>
#include <list>
#include <memory>

#include "PlatformSpecific/PlatformSpecific.h"

#include "ArchiveReader.hpp"
#include "ArchivePacker.hpp"

#include "TestPerformance.hpp"


using namespace std;

void testPackerSerialize();
void testReaderDeserialize();
void testReaderRead();

void testPerformance();

int main(int argc, const char * argv[]) {
    
    // insert code here...
    std::cout << "Hello, World!\n";
    
//    testPackerSerialize();
//    testReaderDeserialize();
//    testReaderRead();
    testPerformance();
    
    system("pause");
    
    return 0;
}

void testPackerSerialize()
{
    cout<<"======= 测试ArchivePacker ==========="<<endl;
    
    // 输入路径
    cout<<"输入要打包文件或文件夹路径: "<<endl;
    string inputPath;
    cin>>inputPath;
    
    // 创建资源包
    unique_ptr<ArchivePacker> packerPtr(ArchivePacker::createPacker(1));
    if(packerPtr != NULL)
    {
        // 添加路径到资源包
        if(packerPtr->addFileOrFolderToArchive(inputPath))
        {
            // 序列化到文件
            cout<<"输入要保存的资源包路径："<<endl;
            string archivePath;
            cin>>archivePath;
            if(packerPtr->serializeToFile(archivePath))
            {
                cout<<"序列化成功！"<<endl;
            }else
            {
                cout<<"序列化失败！"<<endl;
            }
        }else
        {
            cout<<"错误：testPackerSerialize():添加路径:"<<inputPath<<endl;
        }
    }
}

void testReaderDeserialize()
{
    cout<<"============= 测试ArchiveReader =========="<<endl;
    
    cout<<"输入资源包路径:"<<endl;
    string archivePath;
    cin>>archivePath;
    
    unique_ptr<ArchiveReader> readerPtr(ArchiveReader::createReader(archivePath));
    if(readerPtr != NULL)
    {
        readerPtr->debugPrint();
    }else
    {
        cout<<"错误：打开资源包失败："<<archivePath<<endl;
    }
}

void testReaderRead()
{
    cout<<"============ 测试ArchiveReader read ==============="<<endl;
    
    cout<<"输入资源包路径:"<<endl;
    string archivePath;
    cin>>archivePath;
    
    unique_ptr<ArchiveReader> readerPtr(ArchiveReader::createReader(archivePath));
    if(readerPtr != NULL)
    {
        readerPtr->debugPrint();
        
        // 测试读取资源包中的文件
        cout<<"输入要读取的文件路径："<<endl;
        string filePathToOpen;
        cin>>filePathToOpen;
        
        //
        // 单个读取字符
        //
        int offset = 0;
        string buffStr = "";
        char buff[1];
        int readCount = readerPtr->read(filePathToOpen, buff, offset, 1);
        if(readCount != 0)
        {
            do{
                buffStr.push_back(buff[0]);
                offset += readCount;
                readCount = readerPtr->read(filePathToOpen, buff, offset, 1);
            }while(readCount != 0);
            
            cout<<"文件内容："<<buffStr<<"__!__"<<endl;
        }
        else
        {
            //
            cout<<"读取资源包内文件失败："<<filePathToOpen<<endl;
        }
        
        //
        // 多个字符多个字符的读
        //
        string multiBuffStr = "";
        char multiBuff[6];
        offset = 0;
        
        for(int i=0; i<6; i++)
            multiBuff[i] = 0;
        
        readCount = readerPtr->read(filePathToOpen, multiBuff, offset, 5);
        if(readCount != 0)
        {
            do{
                multiBuffStr = multiBuffStr + multiBuff;
                offset += readCount;
                
                for(int i=0; i<6; i++)
                    multiBuff[i] = 0;
                
                readCount = readerPtr->read(filePathToOpen, multiBuff, offset, 5);
            }while(readCount != 0);
            
            cout<<"文件内容："<<multiBuffStr<<"__!__"<<endl;
        }else
        {
            cout<<"读取资源包内文件失败："<<filePathToOpen<<endl;
        }
    }else
    {
        cout<<"错误：创建资源包失败!"<<endl;
    }
}

void testPerformance()
{
    
    cout<<"输入日志文件路径："<<endl;
    string logFilePath;
    cin>>logFilePath;
    
    TestPerformance testPerform(logFilePath);
    testPerform.run();
}



