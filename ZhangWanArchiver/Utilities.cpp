
#include "Utilities.h"
#include <string>

using namespace std;

int Utilities::getExtraByteCountIn4BAlign(int curFileSize)
{
	int remainder = curFileSize % 8;    // 除8后的余数
	return remainder == 0? 0 : 8 - remainder;
}

