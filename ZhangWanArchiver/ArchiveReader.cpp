//
//  ArchiveReader.cpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "ArchiveReader.hpp"
#include "PlatformSpecific/PlatformSpecific.h"
#include "ArchiveV1Reader.hpp"
#include <iostream>

using namespace std;

ArchiveReader::~ArchiveReader()
{
    if(archiveCFile != NULL)
    {
        fclose(archiveCFile);
        archiveCFile = NULL;
    }
}

void ArchiveReader::debugPrint()
{
    for(FileInArchiveMap::iterator it = fileInArchiveMap.begin();
        it != fileInArchiveMap.end();
        it++)
    {
        (*it).second.debugPrint();
    }
}

const string& ArchiveReader::getArchiveFilePath()
{
    return archiveFilePath;
}

// 得到文件包中的所有文件路径
void ArchiveReader::getAllFilePath(list<string>& result)
{
    for(FileInArchiveMap::const_iterator it = fileInArchiveMap.cbegin();
        it != fileInArchiveMap.cend();
        it++)
    {
        result.push_back((*it).first);
    }
}

// 文件路径是否在资源包中存在
bool ArchiveReader::isFileInArchive(const string& filePath)
{
    if(fileInArchiveMap.count(filePath) != 0)
        return true;
    
    return false;
}

// 得到符合正则表达式expession的，文件路径
void ArchiveReader::getAllFilesByPattern(const regex& expression, list<string>& result)
{
    for(FileInArchiveMap::iterator it = fileInArchiveMap.begin();
        it != fileInArchiveMap.cend(); it++)
    {
        const string& filePath = (*it).first;
        if(regex_match(filePath, expression))
            result.push_back(filePath);
    }
}

FileInArchive* ArchiveReader::getFileInArchiveByPath(const string& filePath)
{
    if(fileInArchiveMap.count(filePath) != 0)
        return &fileInArchiveMap.at(filePath);
    return NULL;
}

bool ArchiveReader::addFileInArchive(const FileInArchive& fileInArchive)
{
    // 检查是否，路径重复
    if(fileInArchiveMap.count(fileInArchive.getFilePathInUnixFormat()) == 0)
    {
        fileInArchiveMap[fileInArchive.getFilePathInUnixFormat()] = fileInArchive;
        return true;
    }
    
    return false;
}

bool ArchiveReader::preOpenArchiveFile(const std::string& archivePath)
{
    if(archiveFilePath == archivePath)
    {
        return true;
    }
    else
    {
        if(PlatformSpecific::getInstance()->isRegularFile(archivePath))
        {
            FILE* archive = fopen(archivePath.c_str(), "rb");
            if(archive != NULL)
            {
                archiveFilePath = archivePath;
                archiveCFile = archive;
                return true;
            }
        }
    }
    
    return false;
}

int ArchiveReader::read(const std::string& fileName, void* buff, int offset, int count)
{
    lock_guard<recursive_mutex> lock(rmutex);
    
    // 当前的资源包正确打开
    if(archiveCFile != NULL)
    {
        // 文件是否在在资源包中
        if(isFileInArchive(fileName))
        {
            // 得到文件偏移记录
            FileInArchive* fileInArchive = getFileInArchiveByPath(fileName);
            if(fileInArchive != NULL)
            {
                int fileDataOffset = fileInArchive->getFileDataOffset();
                int fileLength = fileInArchive->getFileLength();
                int fileEnd = fileDataOffset + fileLength;
                
                // 判断，文件偏移 + offset，没有超出位置
                if(fileDataOffset + offset < fileEnd)
                {
                    // seek到 文件偏移+offset位置
                    if(0 == fseek(archiveCFile, fileDataOffset + offset, SEEK_SET))
                    {
                        // 判断 读取的大小，文件偏移+offset+count 有没有超过文件大小
                        if(fileDataOffset + offset + count > fileEnd)
                        {
                            count = fileEnd - fileDataOffset - offset;
                        }
                        
                        // 读取文件内容，并返回
                        int readResult = fread(buff, 1, count, archiveCFile);
                        if(count == readResult)
                        {
                            // 读取成功
                            return count;
                        }else
                        {
                            cout<<"错误：ArchiveReader::read :count="<<count<<"\t"<<
                            "readResult="<<readResult<<endl;
                        }
                    }
                    else
                    {
                        cout<<"错误：ArchiveReader::read : seek错误"<<endl;
                    }
                }else
                {
                    cout<<"info：ArchiveReader::read: 到达文件末尾"<<endl;
                }
            }else
            {
                cout<<"错误：ArchiveReader::read: 文件偏移记录不存在"<<endl;
            }
        }else
        {
            cout<<"错误：ArchiveReader::read: 文件偏移记录不存在"<<endl;
        }
    }else
    {
        cout<<"错误：ArchiveReader::read 资源包没有打开"<<endl;
    }

    return 0;
}

ArchiveReader* ArchiveReader::createReader(const string& archivePath)
{
    ArchiveV1Reader* reader = NULL;
    
    // 打开文件
    if(PlatformSpecific::getInstance()->isRegularFile(archivePath))
    {
        ifstream fileStream;
        fileStream.open(archivePath.c_str(), ifstream::in | ifstream::binary);
        if(fileStream.good())
        {
            // 读取版本
            int32_t version = 0;
            if(!PlatformSpecific::getInstance()->deserialize(version, fileStream))
                return NULL;
            fileStream.close();
            
            // 选择相应的格式，反序列化
            if(version == 1)
            {
                reader = new ArchiveV1Reader();
                if(!reader->deserializeFromFile(archivePath))
                {
                    delete reader;
                    reader = NULL;
                }
            }
            else if(version == 2)
            {
                // 其他版本
                // ...
            }

            // 预先打开资源包
            if(reader != NULL)
            {
                if(!reader->preOpenArchiveFile(archivePath))
                {
                    delete reader;
                    reader = NULL;
                }
            }
        }
    }
    
    return reader;
}


