//
//  TestPerformance.cpp
//  TestPerformance
//
//  Created by admin on 16/3/22.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "TestPerformance.hpp"
#include <iostream>
#include <fstream>
#include <time.h>
#include <memory>
#include <functional>

#include "stdio.h"
//#include "Archive.h"

using namespace std;

TestPerformance::TestPerformance(const string& logFilePath):
                        performResult(logFilePath)
{
    
}

bool TestPerformance::readFilePathesInConfigFile(const string& configPath,
                                                 list<string>& outFilePathList)
{
    // 打开文件
    ifstream configFile;
    configFile.open(configPath);
    if(configFile.good())
    {
        // 读进所有文件路径
        while(!configFile.eof())
        {
            string filePath;
            configFile>>filePath;
            
            if(filePath[0] != '#' && filePath != "")
                outFilePathList.push_back(filePath);
        }
        
        return true;
    }
    
    return false;
}

bool TestPerformance::testReadPerformance(
                                          const list<string>& fileToParse,
                                          const string& timeTypeName,
                                          function<bool(const string&,
                                                        int& openTime,
                                                        int& readTime,
                                                        int& closeTime)> funcToReadFile)
{
    // 日志中打印，要测试的函数的开头信息
    string title = "=============== test " + timeTypeName + " ==========";
    performResult.log(title);
    
    if(!funcToReadFile)
        return false;
    
    // 遍历所有文件
    int i = 0;
    for(list<string>::const_iterator it = fileToParse.begin();
        it != fileToParse.end();
        it++)
    {
        i++;
        const string& filePath = *it;
        
        // 记录开始时间
        clock_t startTime = clock();
        
        // 读文件
        int openTime = 0;
        int readTime = 0;
        int closeTime = 0;
        bool correctProcessed = funcToReadFile(filePath, openTime, readTime, closeTime);
        
        if(!correctProcessed)
        {
            string msg;
            msg += "错误，解析文件";
            msg += filePath;
            performResult.log(msg);
            return false;
        }
        
        // 统计关闭时间
        clock_t endTime = clock();
        clock_t usedTime = endTime - startTime;
        
        // 记录
        performResult.recordTime(filePath,
                                 timeTypeName,
                                 usedTime,
                                 openTime,
                                 readTime,
                                 closeTime);
        
        // 日志输出结果
        stringstream msg;
        int totalTime = openTime + readTime + closeTime;
        msg<<timeTypeName<<": "<<usedTime<<"\t"<<
        "T:"<<totalTime<<"\t"<<
        "O:"<<openTime<<"\t"<<
        "R:"<<readTime<<"\t"<<
        "C:"<<closeTime<<"\t"<<
        "No.:"<<i<<"\t\t"<<
        "file: "<<filePath;
        string msgStr = msg.str();
        performResult.log(msgStr);
    }
    
    return true;
}

// c 函数测试
bool TestPerformance::cGetCReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    clock_t openStart = clock();
    FILE* file = fopen(filePath.c_str(), "rb");
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp;
    
    clock_t readStart = clock();
    if(NULL != file)
    {
        while(0 == feof(file))
        {
            tmp = fgetc(file);
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        fclose(file);
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::cGetSReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    clock_t openStart = clock();
    FILE* file = fopen(filePath.c_str(), "rb");
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp[1024];
    
    clock_t readStart = clock();
    if(NULL != file)
    {
        while(0 == feof(file))
        {
            fgets(tmp, readLineSizeConfig, file);
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        fclose(file);
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::cReadReadFile_useCountParameter(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    clock_t openStart = clock();
    FILE* file = fopen(filePath.c_str(), "rb");
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp[1024];
    
    clock_t readStart = clock();
    if(NULL != file)
    {
        while(0 == feof(file))
        {
            fread(tmp, 1, readSizeConfig, file);
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        fclose(file);
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::cReadReadFile_useSizeParameter(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    clock_t openStart = clock();
    FILE* file = fopen(filePath.c_str(), "rb");
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp[1024];
    
    clock_t readStart = clock();
    if(NULL != file)
    {
        while(0 == feof(file))
        {
            fread(tmp, readSizeConfig, 1, file);
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        fclose(file);
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::testCGetC(const list<string>& fileToParse)
{
    bool result = testReadPerformance(fileToParse,
                                      "c_getc",
                                      bind(&TestPerformance::cGetCReadFile,
                                           this,
                                           placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

bool TestPerformance::testCGetS(const list<string>& fileToParse)
{
    stringstream readLineSizeConfigStr;
    readLineSizeConfigStr<<readLineSizeConfig;
    bool result = testReadPerformance(fileToParse,
                                      "c_gets-" + readLineSizeConfigStr.str(),
                                      bind(&TestPerformance::cGetSReadFile,
                                           this,
                                           placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

bool TestPerformance::testCRead_useCountParameter(const list<string>& fileToParse)
{
    stringstream readSizeConfigStr;
    readSizeConfigStr<<readSizeConfig;
    bool result = testReadPerformance(fileToParse,
                                      "c_read_use-count-" + readSizeConfigStr.str(),
                                      bind(&TestPerformance::cReadReadFile_useCountParameter,
                                           this, placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

bool TestPerformance::testCRead_useSizeParameter(const list<string>& fileToParse)
{
    stringstream readSizeConfigStr;
    readSizeConfigStr<<readSizeConfig;
    bool result = testReadPerformance(fileToParse,
                                      "c_read_use-size-" + readSizeConfigStr.str(),
                                      bind(&TestPerformance::cReadReadFile_useSizeParameter,
                                           this, placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

// c++ 函数测试
bool TestPerformance::cppGetReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    // 打开文件
    clock_t openStart = clock();
    ifstream file;
    file.open(filePath.c_str(), ifstream::in);
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp;
    
    clock_t readStart = clock();
    if(file.good())
    {
        // 读取文件内容
        while(!file.eof())
        {
            tmp = file.get();
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        file.close();
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::cppGetLineReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    // 打开文件
    clock_t openStart = clock();
    ifstream file;
    file.open(filePath.c_str(), ifstream::in);
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp[1024];
    
    clock_t readStart = clock();
    if(file.good())
    {
        // 读取文件内容
        while(!file.eof())
        {
            file.getline(tmp, readLineSizeConfig);
            
            // 测试文件指针是否有往前走
            long curPos = 0;
            curPos = file.tellg();
            int i;
            i++;
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        file.close();
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::cppReadReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    // 打开文件
    clock_t openStart = clock();
    ifstream file;
    file.open(filePath.c_str(), ifstream::in);
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    char tmp[1024];
    
    clock_t readStart = clock();
    if(file.good())
    {
        // 读取文件内容
        while(!file.eof())
        {
            file.read(tmp, readSizeConfig);
        }
        clock_t readEnd = clock();
        readTime = readEnd - readStart;
        
        clock_t closeStart = clock();
        file.close();
        clock_t closeEnd = clock();
        closeTime = closeEnd - closeStart;
        
        return true;
    }
    return false;
}

bool TestPerformance::testCppGet(const list<string>& fileToParse)
{
    bool result = testReadPerformance(fileToParse,
                                      "cpp_get",
                                      bind(&TestPerformance::cppGetReadFile,
                                           this,
                                           placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

bool TestPerformance::testCppGetLine(const list<string>& fileToParse)
{
    stringstream readLineSizeConfigStr;
    readLineSizeConfigStr<<readLineSizeConfig;
    bool result = testReadPerformance(fileToParse,
                                      "cpp_getLine-" + readLineSizeConfigStr.str(),
                                      bind(&TestPerformance::cppGetLineReadFile,
                                           this,
                                           placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

bool TestPerformance::testCppRead(const list<string>& fileToParse)
{
    stringstream readSizeConfigStr;
    readSizeConfigStr<<readSizeConfig;
    bool result = testReadPerformance(fileToParse,
                                      "cpp_read-" + readSizeConfigStr.str(),
                                      bind(&TestPerformance::cppReadReadFile,
                                           this,
                                           placeholders::_1,
                                           placeholders::_2,
                                           placeholders::_3,
                                           placeholders::_4));
    return result;
}

// archive函数测试
bool TestPerformance::archiveReadReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime)
{
    clock_t openStart = clock();
    clock_t openEnd = clock();
    openTime = openEnd - openStart;
    
    
    
    char multiBuff[1024];
    int offset = 0;
    
    //
    // 多个字符多个字符的读
    //
    clock_t readStart = clock();
    if(archivePtr != NULL)
    {

        int readCount = archivePtr->read(filePath, multiBuff, offset, readSizeConfig);
        if(readCount != 0)
        {
            do{
                offset += readCount;
                readCount = archivePtr->read(filePath, multiBuff, offset, readSizeConfig);
            }while(readCount != 0);
            
            clock_t readEnd = clock();
            readTime = readEnd - readStart;
            
            clock_t closeStart = clock();
            clock_t closeEnd = clock();
            closeTime = closeEnd - closeStart;
            
            return true;
        }else
        {
            string msg;
            msg = "读取资源包内文件失败：" + filePath;
            performResult.log(msg);
        }
    }else
    {
        string msg;
        msg = "读取资源包内文件失败：" + filePath;
        performResult.log(msg);
    }
    
    return false;
}

bool TestPerformance::testArchiveRead(const string& archivePath, const list<string>& fileToParse)
{
    bool result = false;
    archivePtr = unique_ptr<ArchiveReader>(ArchiveReader::createReader(archivePath));
    if(archivePtr != NULL)
    {
        stringstream readSizeConfigStr;
        readSizeConfigStr<<readSizeConfig;
        result = testReadPerformance(fileToParse,
                                          "archive_read-" + readSizeConfigStr.str(),
                                          bind(&TestPerformance::archiveReadReadFile,
                                               this,
                                               placeholders::_1,
                                               placeholders::_2,
                                               placeholders::_3,
                                               placeholders::_4));
    }
    
    if(result == false)
    {
        cout<<"错误archiveRead"<<endl;
    }
    
    return result;
}

int TestPerformance::run()
{
    cout<<"输入配置文件路径："<<endl;
    string configFilePath;
    cin>>configFilePath;
    
    // 读进所有文件路径
    readFilePathesInConfigFile(configFilePath, filePathList);
    cout<<"文件路径个数："<<filePathList.size()<<endl;
    
    // 获得资源包路径
    cout<<"输入资源包路径:"<<endl;
    string archivePath;
    cin>>archivePath;
    
    // 函数参数配置
    readLineSizeConfig = 200;
    readSizeConfig = 200;
    
//    // 测试native c api读效率
//    testCGetC(filePathList);
//    testCGetS(filePathList);
    testCRead_useCountParameter(filePathList);
//    testCRead_useSizeParameter(filePathList);
//
    
//    // 测试native cpp api读效率
//    testCppGet(filePathList);
//    //    testCppGetLine(filePathList);
//    testCppRead(filePathList);
    
    //
    // 转换成archivePath，内部路径
    //
    cout<<"输入资源根目录："<<endl;
    string archiveRootPath;
    cin>>archiveRootPath;
    
    convertPathToArchiveInternalFormat(filePathList, archiveRootPath);

    // 测试Archive 读效率
    testArchiveRead(archivePath, filePathList);
    
    // 打印时间统计信息
    performResult.printSumUpResult();
    
    return 0;
}

void TestPerformance::convertPathToArchiveInternalFormat(
                                                         list<string>& filesToParse,
                                                         const string& archiveRootPath)
{
    string normArchiveRootPath = archiveRootPath;
    if(normArchiveRootPath.back() != '/')
        normArchiveRootPath = normArchiveRootPath + '/';
    
    int normArchiveRootPathLength = normArchiveRootPath.length();
    
    for(list<string>::iterator it = filesToParse.begin();
        it != filesToParse.end();
        it++)
    {
        string& filePath = (*it);
        filePath.replace(0, normArchiveRootPathLength, "");
    }
}





