//
//  PerformanceResult.hpp
//  TestPerformance
//
//  Created by admin on 16/3/22.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef PerformanceResult_hpp
#define PerformanceResult_hpp

#include <stdio.h>
#include <time.h>
#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>


struct MethodSumUpTime
{
    std::string methodName;
    int fileCount;                  // 统计的文件数量
    
    clock_t totalTotalTime;         // 所有文件时间加起来的统计
    int totalOpenTime;
    int totalReadTime;
    int totalCloseTime;
    
    float averageTotalTime;       // 文件平均的时间
    float averageOpenTime;
    float averageReadTime;
    float averageCloseTime;
    
    MethodSumUpTime()
    {
        methodName = "";
        fileCount = 0;
        totalTotalTime = 0;
        totalReadTime = 0;
        totalCloseTime = 0;
    }
    
    void calculateAverage()
    {
        averageTotalTime = (float)totalTotalTime / (float)fileCount;
        averageOpenTime = (float)totalOpenTime / (float)fileCount;
        averageReadTime = (float)totalReadTime / (float)fileCount;
        averageCloseTime = (float)totalCloseTime / (float)fileCount;
    }
};

struct FileDetailTime
{
    std::string filePath;        // 文件路径名称
    std::string methodName;    // 使用的读方法名称
    clock_t totalTime;
    int openTime;
    int readTime;
    int closeTime;
};

// filePath/timeTypeName , FileDetailTime
typedef std::map<std::pair<std::string, std::string>, FileDetailTime> FileDetailTimeMap;
// methodName, MethodSumUpTime
typedef std::map<std::string, MethodSumUpTime> MethodSumUpTimeMap;

class PerformanceResult
{
private:
    // 文件的某种读方法，使用的访问时间
    FileDetailTimeMap fileDetailTimeMap;
    // 读方法，的时间总结
    MethodSumUpTimeMap methodSumUpTimeMap;
    
    // 日志文件
    std::string logFilePath;
    std::ofstream logFile;
    
public:
    PerformanceResult(const std::string& logFilePath);
    ~PerformanceResult();
    
    void recordTime(const std::string& filePath,
                    const std::string& timeTypeName,
                    clock_t time,
                    int openTime,
                    int readTime,
                    int closeTime);
    
    void printSumUpResult();
    
    void log(const std::string& msg);
};

#endif /* PerformanceResult_hpp */
