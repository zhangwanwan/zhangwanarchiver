//
//  PerformanceResult.cpp
//  TestPerformance
//
//  Created by admin on 16/3/22.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "PerformanceResult.hpp"

using namespace std;

PerformanceResult::PerformanceResult(const string& logFilePath)
{
    this->logFilePath = logFilePath;
}

PerformanceResult::~PerformanceResult()
{
    if(logFile.is_open())
        logFile.close();
}

void PerformanceResult::recordTime(const string& filePath,
                                   const string& methodName,
                                   clock_t time,
                                   int openTime,
                                   int readTime,
                                   int closeTime
                                   )
{
    // 创建，读方法的SumUpTime结构
    MethodSumUpTime sumUpTime;
    sumUpTime.methodName = methodName;
    methodSumUpTimeMap[methodName] = sumUpTime;
    
    // 纪录读取文件的相应
    // 时间类型
    // 打开、读取、关闭时间
    pair<string, string> key(filePath, methodName);
    FileDetailTime& fileDetailTime = fileDetailTimeMap[key];
    fileDetailTime.filePath = filePath;
    fileDetailTime.methodName = methodName;
    fileDetailTime.totalTime = time;
    fileDetailTime.openTime = openTime;
    fileDetailTime.readTime = readTime;
    fileDetailTime.closeTime = closeTime;
}

void PerformanceResult::printSumUpResult()
{
    // 输出各类读方法的,
    // method-name, aT:time aO:time aR:time aC:time,
    // ...
    // method-name, mT:time mOTime mR:time mC:time
    
    // 遍历所有读方法
    for(MethodSumUpTimeMap::iterator it = methodSumUpTimeMap.begin();
        it != methodSumUpTimeMap.end();
        it++)
    {
        MethodSumUpTime& methodSumUpTime = (*it).second;
        
        // 遍历所有用此方法打开的文件
        for(FileDetailTimeMap::iterator it = fileDetailTimeMap.begin();
            it != fileDetailTimeMap.end();
            it++)
        {
            // 找到
            FileDetailTime& fileDetailTime = (*it).second;
            if(fileDetailTime.methodName == methodSumUpTime.methodName)
            {
                // 更新统计个数，总时间
                methodSumUpTime.fileCount ++;
                methodSumUpTime.totalTotalTime += fileDetailTime.totalTime;
                methodSumUpTime.totalOpenTime += fileDetailTime.openTime;
                methodSumUpTime.totalReadTime += fileDetailTime.readTime;
                methodSumUpTime.totalCloseTime += fileDetailTime.closeTime;
            }
        }
        
        // 计算平均时间
        methodSumUpTime.calculateAverage();
        
        // 打印输出
        stringstream msg;
        msg<<
        "aT: "<<methodSumUpTime.averageTotalTime<<"\t"<<
        "aO: "<<methodSumUpTime.averageOpenTime<<"\t"<<
        "aR: "<<methodSumUpTime.averageReadTime<<"\t"<<
        "aC: "<<methodSumUpTime.averageCloseTime<<"\t"<<
        "Count: "<<methodSumUpTime.fileCount<<"\t"<<
        methodSumUpTime.methodName<<"\t";
        
        log(msg.str());
    }
}

void PerformanceResult::log(const string& msg)
{
    cout<<msg<<endl;
    
    // 同时写入文件
    if(!logFile.is_open())
    {
        logFile.open(logFilePath, ofstream::out);
        if(!logFile.good())
        {
            cout<<"错误：log打开日志文件！"<<endl;
            return;
        }
    }
    
    logFile<<msg<<endl;
}
