//
//  TestPerformance.hpp
//  TestPerformance
//
//  Created by admin on 16/3/22.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef TestPerformance_hpp
#define TestPerformance_hpp

#include <stdio.h>
#include <string>
#include <list>

#include "PerformanceResult.hpp"
//#include "Archive.h"
#include "ArchiveReader.hpp"


using namespace std;

class TestPerformance
{
    
private:
    std::list<std::string> filePathList;
    PerformanceResult performResult;
    

    int readLineSizeConfig;
    int readSizeConfig;
    
    // archive
    unique_ptr<ArchiveReader> archivePtr;
   
private:
    
    // 从config文件中，读取要测试的文件列表
    bool readFilePathesInConfigFile(const std::string& configPath, std::list<std::string>& outFilePathList);
    
    // 测试函数性能模板函数
    // 遍历所有文件，
    // 使用相应的读函数，读取文件
    // 计时，
    // 纪录log
    bool testReadPerformance(
                             const std::list<string>& fileToParse,
                             const std::string& timeTypeName,
                             std::function<bool(const string&,
                                           int& openTime,
                                           int& readTime,
                                           int& closeTime)> funcToReadFile);
    
    // c 函数测试
    bool cGetCReadFile(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    bool cGetSReadFile(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    bool cReadReadFile_useCountParameter(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    bool cReadReadFile_useSizeParameter(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    
    bool testCGetC(const std::list<std::string>& fileToParse);
    bool testCGetS(const std::list<std::string>& fileToParse);
    bool testCRead_useCountParameter(const std::list<std::string>& fileToParse);
    bool testCRead_useSizeParameter(const std::list<std::string>& fileToParse);
    
    // c++ 函数测试
    bool cppGetReadFile(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    bool cppGetLineReadFile(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    bool cppReadReadFile(const std::string& filePath, int& openTime, int& readTime, int& closeTime);
    
    bool testCppGet(const std::list<std::string>& fileToParse);
    bool testCppGetLine(const std::list<std::string>& fileToParse);
    bool testCppRead(const std::list<std::string>& fileToParse);
    
    // archive函数测试
    bool archiveReadReadFile(const string& filePath, int& openTime, int& readTime, int& closeTime);
    bool testArchiveRead(const string& archivePath, const list<string>& fileToParse);
    
    // 转换绝对路径成archive中的内部路径
    void convertPathToArchiveInternalFormat(std::list<std::string>& filesToParse,
                                            const std::string& archiveRootPath);
    
public:
    TestPerformance(const std::string& logFilePath);
    int run();
};

#endif /* TestPerformance_hpp */
