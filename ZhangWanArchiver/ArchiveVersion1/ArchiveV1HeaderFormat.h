//
//  ArchiveFormatHeader.h
//  ZhangWanArchiver
//
//  Created by admin on 16/3/17.
//  Copyright (c) 2016年 admin. All rights reserved.
//

#ifndef ZhangWanArchiver_ArchiveFormatHeader_h
#define ZhangWanArchiver_ArchiveFormatHeader_h

#include <map>
#include <list>
#include "ArchiveV1FileRecordFormat.h"

// 资源包文件，文件头
// 用来根据文件路径索引文件，得到文件数据的偏移

typedef std::list<ArchiveV1FileRecordFormat> FileRecordList;

class ArchiveV1HeaderFormat
{    
public:
    int32_t formatVersion;              // 资源包格式版本号
	int32_t recordsCount;               // 文件索引条目个数
    FileRecordList fileRecordList;      // 文件索引条目
    
    bool serializeToFile(std::ofstream& stream) const;
    bool deserializeFromFile(std::ifstream& stream);
    void debugPrint() const;
    int getSerializeLength() const;
	int getSerializeLengthWith4bAlign() const;
};

#endif
