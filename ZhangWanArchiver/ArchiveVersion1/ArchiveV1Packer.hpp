//
//  ArchiveV1Packer.hpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef ArchiveV1Packer_hpp
#define ArchiveV1Packer_hpp

#include <stdio.h>
#include "ArchivePacker.hpp"
#include "ArchiveV1HeaderFormat.h"

class ArchiveV1Packer : public ArchivePacker
{
private:
    void buildHeaderFormat(ArchiveV1HeaderFormat& header);
    void updateRecordFileDataOffset(ArchiveV1HeaderFormat& header);
    bool serializeAllFileDataAfterHeader(const ArchiveV1HeaderFormat& header, std::ofstream& stream);
    
    // 序列化之后占用文件大小
    int getSerializeLength();
    // 序列化之后占用文件大小，考虑对齐
    int getSerializeLengthWith4BAlign();
    
    // 当次record记录的文件写入磁盘的时候，需要多少字节完成4B对齐
    int getExtra4bAlignByteCountForSerializeHeader(const ArchiveV1HeaderFormat& header);
    int getExtra4bAlignByteCountForSerializeFile(const ArchiveV1FileRecordFormat& record) ;
    int getExtra4bAlignByteCount(std::ifstream& stream);
    
public:
    // 根据资源包头部结构记录的文件地址，写入文件内容到资源包
    virtual bool serializeToFile(const std::string& archivePath);
    virtual bool serializeToFile(std::ofstream& stream);
};

#endif /* ArchiveV1Packer_hpp */
