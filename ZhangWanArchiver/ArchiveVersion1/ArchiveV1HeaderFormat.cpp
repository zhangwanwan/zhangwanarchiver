
#include <iostream>

#include "ArchiveV1HeaderFormat.h"
#include "PlatformSpecific.h"
#include "Utilities.h"

using namespace std;

bool ArchiveV1HeaderFormat::serializeToFile(ofstream& stream) const
{
    // 序列化版本号字段
    if(!PlatformSpecific::getInstance()->serialize(formatVersion, stream))
        return false;
    
    // 序列化record个数
    int32_t recordCount = (int32_t)fileRecordList.size();
    if(!PlatformSpecific::getInstance()->serialize(recordCount, stream))
        return false;
    
    // 序列化records字段
    for(FileRecordList::const_iterator it = fileRecordList.begin();
		it != fileRecordList.end();
		it++)
    {
        const ArchiveV1FileRecordFormat& record = (*it);
        if(!record.serializeToFile(stream))
            return false;
    }
    
    // 序列化4b对齐字节
    int extraByteCount = Utilities::getExtraByteCountIn4BAlign(getSerializeLength());
    for(int i=0; i<extraByteCount; i++)
    {
        stream.put('\0');
        if(!stream.good())
            return false;
    }
    
    return true;
}

int ArchiveV1HeaderFormat::getSerializeLength() const
{
    int length = 0;
    
    // 序列化版本号字段
    length += sizeof(formatVersion);
    
    // 序列化record个数
    length += sizeof(int32_t);
    
    // 序列化records字段
    for(FileRecordList::const_iterator it = fileRecordList.begin();
		it != fileRecordList.end();
		it++)
    {
        const ArchiveV1FileRecordFormat& record = (*it);
        length += record.getSerializaLength();
    }
    
    return length;
}

int ArchiveV1HeaderFormat::getSerializeLengthWith4bAlign() const
{
    int lengthWithoutAlign = getSerializeLength();
    return lengthWithoutAlign + Utilities::getExtraByteCountIn4BAlign(lengthWithoutAlign);
}

bool ArchiveV1HeaderFormat::deserializeFromFile(ifstream& stream)
{
    // 反序列化版本号字段
    int32_t deserializedVersion = 0;
    if(!PlatformSpecific::getInstance()->deserialize(deserializedVersion, stream))
        return false;
    
	// 检查版本是否一致
    if(deserializedVersion != formatVersion)
		return false;
	
	// 反序列化record个数
	int32_t recordCount = 0;
	if(!PlatformSpecific::getInstance()->deserialize(recordCount, stream))
		return false;

	// 反序列化各个ArchiveFormatSimpleIndexRecord
	for(int i=0; i<recordCount; i++)
	{
		ArchiveV1FileRecordFormat newRecord;
		if(!newRecord.deserializeFromFile(stream))
			return false;

		fileRecordList.push_back(newRecord);
	}

    return true;
}

void ArchiveV1HeaderFormat::debugPrint() const
{
    cout<<"version: "<<formatVersion<<endl<<
    "record count: "<<fileRecordList.size()<<endl<<
    "records:"<<endl;
    
    for(FileRecordList::const_iterator it = fileRecordList.begin();
		it != fileRecordList.end();
		it++)
    {
        const ArchiveV1FileRecordFormat& record = (*it);
        record.debugPrint();
    }
}
