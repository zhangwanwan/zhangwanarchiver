//
//  ArchiveFormatSimpleIndexRecord.h
//  ZhangWanArchiver
//
//  Created by admin on 16/3/17.
//  Copyright (c) 2016年 admin. All rights reserved.
//

#ifndef ZhangWanArchiver_ArchiveFormatSimpleIndexRecord_h
#define ZhangWanArchiver_ArchiveFormatSimpleIndexRecord_h

#include <string>
#include <cstdint>

// 简单的根据路径的文件索引结构
class ArchiveV1FileRecordFormat
{
public:
    std::string filePathUnixFormat;           // 文件路径
    int32_t fileDataOffset;         // 文件内容在资源包中起始偏移
    int32_t fileLength;             // 文件内容长度
    
    ArchiveV1FileRecordFormat();
    ArchiveV1FileRecordFormat(const std::string& filePath, int32_t fileDataOffset, int32_t fileLength);
    
    bool serializeToFile(std::ofstream& stream) const;
    bool deserializeFromFile(std::ifstream& stream);
    void debugPrint() const;
    int getSerializaLength() const;
    
    ArchiveV1FileRecordFormat& operator=(const ArchiveV1FileRecordFormat& other);
};

#endif
