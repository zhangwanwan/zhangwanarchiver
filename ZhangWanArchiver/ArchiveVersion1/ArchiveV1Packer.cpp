//
//  ArchiveV1Packer.cpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "ArchiveV1Packer.hpp"

#include <iostream>
//#include "ArchiveV1.h"

#include "PlatformSpecific.h"
#include "Utilities.h"

using namespace std;

void ArchiveV1Packer::buildHeaderFormat(ArchiveV1HeaderFormat& header)
{
    const FileInArchiveMap& fileInArchiveMap = getAllFileInArchive();
    
    // 填写header中字段
    header.formatVersion = 1;
    header.recordsCount = (int32_t)fileInArchiveMap.size();
    
    // 创建FileRecord
    for(FileInArchiveMap::const_iterator it = fileInArchiveMap.begin();
        it != fileInArchiveMap.end();
        it++)
    {
        //
        const FileInArchive& fileInArchive = (*it).second;
        ArchiveV1FileRecordFormat record;
        record.filePathUnixFormat = fileInArchive.getFilePathInUnixFormat();
        //record.fileDataOffset = fileInArchive.getFileDataOffset();
        record.fileLength = fileInArchive.getFileLength();
        header.fileRecordList.push_back(record);
    }
    
    // 更新FileRecord记录的文件偏移
    updateRecordFileDataOffset(header);
}

// 根据迭代器顺序，更新文件索引结构中的偏移值
void ArchiveV1Packer::updateRecordFileDataOffset(ArchiveV1HeaderFormat& header)
{
    // 初始文件头大小
    int curFileSize = header.getSerializeLengthWith4bAlign();
    
    for(FileRecordList::iterator it = header.fileRecordList.begin();
        it != header.fileRecordList.end();
        it++)
    {
        ArchiveV1FileRecordFormat& record = (*it);
        record.fileDataOffset = curFileSize;
        curFileSize += record.fileLength;
        // 增加字节成，4字节对齐
        curFileSize += getExtra4bAlignByteCountForSerializeFile(record);
    }
}

bool ArchiveV1Packer::serializeAllFileDataAfterHeader(
                                                const ArchiveV1HeaderFormat& header,
                                                ofstream& stream)
{
    // 根据迭代器顺序，依次打开各文件
    int curFileCount = 0;
    for(FileRecordList::const_iterator it = header.fileRecordList.begin();
        it != header.fileRecordList.end();
        it++)
    {
        curFileCount ++;
        const ArchiveV1FileRecordFormat& record = (*it);
        
#ifdef WINDOWS
        string fullFilePath = rootFilePath + PlatformSpecific::getInstance()->getWindowsPathFormat(record.filePathUnixFormat);
#endif
        
#ifdef UNIX_LIKE
        string fullFilePath = rootFilePath + record.filePathUnixFormat;
#endif
        
        ifstream istream;
        istream.open(fullFilePath.c_str(), istream::in | istream::binary);
        if(istream.good())
        {
            // 写入整个文件内容
            streambuf* buf = istream.rdbuf();
            stream<<buf;
            if(!stream.good())
                return false;
            
            // 当前文件不是最后一个文件
            // 写入额外4B对齐字节
            if(curFileCount != header.fileRecordList.size())
            {
                // 得到要4B自己的字节数
                int extraByteCount = getExtra4bAlignByteCountForSerializeFile(record);
                // 写入额外的4B对齐字节
                for(int i=0; i<extraByteCount; i++)
                {
                    stream.put('\0');
                    if(!stream.good())
                        return false;
                }
            }
        }else
        {
            return false;
        }
    }
    
    return true;
}

bool ArchiveV1Packer::serializeToFile(const string& archivePath)
{
    bool result = false;
    
    // 打开文件
    ofstream fileStream;
    fileStream.open(archivePath, ofstream::out | ofstream::binary);
    if(fileStream.good())
    {
        if(serializeToFile(fileStream))
            result = true;
        else
        {
            cout<<"错误：ArchiveV1::serializeToFile:序列化失败！："<<endl;
        }
        fileStream.close();
    }else
    {
        cout<<"错误：ArchiveV1::serializeToFile:打开文件失败！："<<archivePath<<endl;
    }
    
    return result;
}

bool ArchiveV1Packer::serializeToFile(ofstream& stream)
{
    // 构建文件头部
    ArchiveV1HeaderFormat header;
    buildHeaderFormat(header);
    
    // 先书写文件头部结构
    if(!header.serializeToFile(stream))
    {
        cout<<"错误：ArchiveV1::serializeToFile:序列化头部失败！"<<endl;
        return false;
    }
    
    // 将记录的文件内容依次，写入头部结构之后
    if(!serializeAllFileDataAfterHeader(header, stream))
    {
        cout<<"错误：ArchiveV1::serializeToFile:序列化文件失败！"<<endl;
        return false;
    }
    
    return true;
}

int ArchiveV1Packer::getSerializeLength()
{
    // 构建文件头部
    ArchiveV1HeaderFormat header;
    buildHeaderFormat(header);
    
    int length = 0;
    
    // 序列化头部
    length += header.getSerializeLength();
    
    // 序列化文件内容
    for(FileRecordList::iterator it = header.fileRecordList.begin();
        it != header.fileRecordList.end();
        it++)
    {
        ArchiveV1FileRecordFormat record;
        record = (*it);
        length += record.fileLength;
    }
    
    return length;
}

int ArchiveV1Packer::getSerializeLengthWith4BAlign()
{
    int length = 0;
    
    // 构建文件头部
    ArchiveV1HeaderFormat header;
    buildHeaderFormat(header);
    
    // 序列化头部
    length += header.getSerializeLengthWith4bAlign();
    
    // 序列化文件内容
    int curFileIndex = 0;
    for(FileRecordList::iterator it = header.fileRecordList.begin();
        it != header.fileRecordList.end();
        it++)
    {
        curFileIndex++;
        ArchiveV1FileRecordFormat record;
        record = (*it);
        length += record.fileLength;
        // 如果不是最后一个文件
        if(curFileIndex != header.fileRecordList.size())
        {
            // 得到要4B自己的字节数
            int extraByteCount = getExtra4bAlignByteCountForSerializeFile(record);
            length += extraByteCount;
        }
    }
    
    return length;
}

int ArchiveV1Packer::getExtra4bAlignByteCountForSerializeHeader(const ArchiveV1HeaderFormat& header)
{
    return Utilities::getExtraByteCountIn4BAlign(header.getSerializeLength());
}

int ArchiveV1Packer::getExtra4bAlignByteCountForSerializeFile(const ArchiveV1FileRecordFormat& record)
{
    int fileEndPos = record.fileDataOffset + record.fileLength - 1;
    int endFileSize = fileEndPos + 1;
    int extraByteCount = Utilities::getExtraByteCountIn4BAlign(endFileSize);
    return extraByteCount;
}


