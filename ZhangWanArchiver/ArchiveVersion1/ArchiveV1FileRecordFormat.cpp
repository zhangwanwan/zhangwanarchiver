
#include <iostream>

#include "ArchiveV1FileRecordFormat.h"
#include "PlatformSpecific.h"

using namespace std;

ArchiveV1FileRecordFormat::ArchiveV1FileRecordFormat()
{
}


ArchiveV1FileRecordFormat::ArchiveV1FileRecordFormat(
                                                               const string& filePath,
                                                               int32_t fileDataOffset,
                                                               int32_t fileLength):
                                        filePathUnixFormat(filePath),
                                        fileDataOffset(fileDataOffset),
                                        fileLength(fileLength)
{
}

bool ArchiveV1FileRecordFormat::serializeToFile(ofstream& stream) const
{
    // 序列化文件路径字段
    for(string::const_iterator it = filePathUnixFormat.begin();
        it != filePathUnixFormat.end();
        it++)
    {
        char c = (*it);
        stream.put(c);
        if(!stream.good())
            return false;
    }
    
    stream.put(0);    // 字符串结束
    if(!stream.good())
        return false;
    
    // 序列化偏移字段
    if(!PlatformSpecific::getInstance()->serialize(fileDataOffset, stream))
        return false;
    
    // 序列化文件长度字段
    if(!PlatformSpecific::getInstance()->serialize(fileLength, stream))
        return false;
    
    return true;
}

int ArchiveV1FileRecordFormat::getSerializaLength() const
{
    int length = 0;
    
    // 序列化文件路径字段
    length += filePathUnixFormat.length();
    length += 1;
    
    // 序列化偏移字段
    length += sizeof(fileDataOffset);
    
    // 序列化文件长度字段
    length += sizeof(fileLength);
    
    return length;
}

bool ArchiveV1FileRecordFormat::deserializeFromFile(ifstream& stream)
{
    // 反序列化文件路径字段
    filePathUnixFormat = "";
    
    char c = stream.get();
    if(!stream.good())  return false;
    
    while(c != 0)
    {
        filePathUnixFormat = filePathUnixFormat + c;
        c = stream.get();
        if(!stream.good())  return false;
    }
    
    // 反序列化偏移字段
    if(!PlatformSpecific::getInstance()->deserialize(fileDataOffset, stream))
        return false;
    
    // 反序列化文件长度字段
    if(!PlatformSpecific::getInstance()->deserialize(fileLength, stream))
        return false;
    
    return true;
}

void ArchiveV1FileRecordFormat::debugPrint() const
{
    cout<<"record path: "<<filePathUnixFormat<<
    " offset: " << fileDataOffset<<
    " length: " << fileLength<<endl;
}

ArchiveV1FileRecordFormat& ArchiveV1FileRecordFormat::operator=(const ArchiveV1FileRecordFormat& other)
{
    filePathUnixFormat = other.filePathUnixFormat;
    fileDataOffset = other.fileDataOffset;
    fileLength = other.fileLength;
    return *this;
}
