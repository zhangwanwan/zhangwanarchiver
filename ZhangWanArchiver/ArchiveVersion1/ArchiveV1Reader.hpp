//
//  ArchiveV1Reader.hpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef ArchiveV1Reader_hpp
#define ArchiveV1Reader_hpp

#include <stdio.h>
#include <string>
#include <fstream>

#include "ArchiveReader.hpp"
#include "ArchiveV1HeaderFormat.h"

class ArchiveV1Reader : public ArchiveReader
{
private:
    
public:
    virtual bool deserializeFromFile(std::ifstream& stream);
    virtual bool deserializeFromFile(const std::string& archivePath);
};

#endif /* ArchiveV1Reader_hpp */
