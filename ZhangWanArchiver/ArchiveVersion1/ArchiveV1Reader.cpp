//
//  ArchiveV1Reader.cpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "ArchiveV1Reader.hpp"
#include "PlatformSpecific.h"

using namespace std;

bool ArchiveV1Reader::deserializeFromFile(const string& archivePath)
{
    // 打开文件
    ifstream fileStream;
    fileStream.open(archivePath.c_str(), ifstream::in | ifstream::binary);
    if(fileStream.good())
    {
        // 保存资源包路径
        if(!deserializeFromFile(fileStream))
            return false;
        fileStream.close();
        return true;
    }
    
    return false;
}

bool ArchiveV1Reader::deserializeFromFile(ifstream& stream)
{
    // 创建文件头
    ArchiveV1HeaderFormat header;
    header.formatVersion = 1;
    
    // 文件头反序列化
    if(!header.deserializeFromFile(stream))
        return false;
    
    // 将数据填写到Archive
    for(FileRecordList::iterator it = header.fileRecordList.begin();
        it != header.fileRecordList.end();
        it++)
    {
        ArchiveV1FileRecordFormat& recordFormat = (*it);
        
        // 创建ArchiveInFile结构
        FileInArchive fileInArchive(
                                    recordFormat.filePathUnixFormat,
                                    recordFormat.fileDataOffset, 
                                    recordFormat.fileLength);
        
        if(!addFileInArchive(fileInArchive))
            return false;
    }
    
    return true;
}