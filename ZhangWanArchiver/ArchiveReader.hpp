//
//  ArchiveReader.hpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef ArchiveReader_hpp
#define ArchiveReader_hpp

#include <stdio.h>
#include <map>
#include <string>
#include <list>
#include <regex>

#include "FileInArchive.h"

typedef std::map<std::string, FileInArchive> FileInArchiveMap;

// 资源包文件格式
class ArchiveReader
{
private:
    std::string archiveFilePath;
    FILE* archiveCFile;
    
protected:
    std::recursive_mutex rmutex;
    FileInArchiveMap fileInArchiveMap;
    
private:
    FileInArchive* getFileInArchiveByPath(const std::string& filePath);
    bool preOpenArchiveFile(const std::string& archivePath);
    
protected:
    bool addFileInArchive(const FileInArchive& fileInArchive);
    virtual bool deserializeFromFile(std::ifstream& stream) = 0;
    virtual bool deserializeFromFile(const std::string& archivePath) = 0;
    
public:
    ~ArchiveReader();
    // 调试打印
    virtual void debugPrint();
    
    // 查询api
    const std::string& getArchiveFilePath();
    void getAllFilePath(std::list<std::string>& outResult);
    bool isFileInArchive(const std::string& filePath);
    void getAllFilesByPattern(const std::regex& expression, std::list<std::string>& outResult);
    
    // 查询文件大小
    // ...
    
    // 读取api
    int read(const std::string& fileName, void* buff, int offset, int count);
    
    // 创建API
    // 返回的ArchiveReader*，需自己管理释放
    static ArchiveReader* createReader(const std::string& archivePath);
    
};

#endif /* ArchiveReader_hpp */
