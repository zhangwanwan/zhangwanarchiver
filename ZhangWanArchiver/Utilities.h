
#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include <string>

class Utilities
{
public:
	// 计算需要增加多少字节，来完成4B对齐
    static int getExtraByteCountIn4BAlign(int curFileSize);
};

#endif

