//
//  ArchivePacker.hpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef ArchivePacker_hpp
#define ArchivePacker_hpp

#include <stdio.h>
#include <map>
#include <string>
#include "FileInArchive.h"

typedef std::map<std::string, FileInArchive> FileInArchiveMap;

// 资源包文件格式
class ArchivePacker
{
protected:
    FileInArchiveMap fileInArchiveMap;
    std::string rootFilePath;
    
private:
    // 将要添加到资源包的文件，添加到头部结构
    bool addRelativeFile(const std::string& relativeFilePath);
    
protected:
    const FileInArchiveMap& getAllFileInArchive() {return fileInArchiveMap;};
    
public:
    // 将要添加到资源包的文件，添加到头部结构
    bool addFileOrFolderToArchive(const std::string folderOrFilePath);
    
    // 序列化到文件
    virtual bool serializeToFile(const std::string& archivePath) = 0;
    
    // 调试打印
    virtual void debugPrint();
    
    // 创建api
    static ArchivePacker* createPacker(int version);
};

#endif /* ArchivePacker_hpp */
