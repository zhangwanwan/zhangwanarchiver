//
//  FileInArchive.h
//  ZhangWanArchiver
//
//  Created by admin on 16/3/17.
//  Copyright (c) 2016年 admin. All rights reserved.
//

#ifndef ZhangWanArchiver_FileInArchive_h
#define ZhangWanArchiver_FileInArchive_h

#include <mutex>
#include <stdio.h>
#include <atomic>
#include <string>

// 资源包内文件记录
class FileInArchive
{
private:
	std::string filePathInUnixFormat;
	int fileDataOffset;
	int fileLength;
    
public:
	FileInArchive();
	FileInArchive(const std::string& filePath, int fileLength);
	FileInArchive(const std::string& filePath, int fileDataOffset, int fileLength);

	// get
	const std::string& getFilePathInUnixFormat() const {return filePathInUnixFormat;};
	int getFileDataOffset() const {return fileDataOffset;}
	int getFileLength() const{return fileLength;}

	void debugPrint();
};

#endif
