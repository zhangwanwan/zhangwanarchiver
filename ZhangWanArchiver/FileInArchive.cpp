
#include "FileInArchive.h"
#include <iostream>
#include "PlatformSpecific.h"

using namespace std;

FileInArchive::FileInArchive():
	filePathInUnixFormat(""),
	fileDataOffset(-1),
	fileLength(0)
{
}

FileInArchive::FileInArchive(const string& filePath, int fileLength)
{
    this->filePathInUnixFormat = PlatformSpecific::getInstance()->getUnixPathFormat(filePath);
	this->fileDataOffset = -1;
	this->fileLength = fileLength;
}

FileInArchive::FileInArchive(const string& filePath, int fileDataOffset, int fileLength)
{
    this->filePathInUnixFormat = PlatformSpecific::getInstance()->getUnixPathFormat(filePath);
	this->fileDataOffset = fileDataOffset;
	this->fileLength = fileLength;
}

void FileInArchive::debugPrint()
{
	cout<<"filePathInUnixFormat: "<<filePathInUnixFormat<<
		" offset: "<<fileDataOffset<<
		" len: "<<fileLength<<endl;
}

