

#include "PlatformSpecificUnixLike.h"

#ifdef UNIX_LIKE

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <iostream>

using namespace std;

char PlatformSpecificUnixLike::getFilePathSeparator()
{
	return '/';
}

bool PlatformSpecificUnixLike::isDirectory(const string& filePath)
{
	struct stat fileAttrib;
	if(0 == stat(filePath.c_str(), &fileAttrib))
	{
		if(S_ISDIR(fileAttrib.st_mode))
			return true;
	}

	return false;
}

bool PlatformSpecificUnixLike::isRegularFile(const string& filePath)
{
	    // 判断文件类型
	    struct stat fileAttrib;
	    if(0 == stat(filePath.c_str(), &fileAttrib))
	    {
	        if(S_ISREG(fileAttrib.st_mode))
	            return true;
	    }

	return false;
}

// 返回的是
bool PlatformSpecificUnixLike::getFilesInFolderRecursively(const string& folderPath, list<string>& filePathes)
{
    // 规范路径/
    char pathSeparator = PlatformSpecific::getInstance()->getFilePathSeparator();
    string normFolderPath = folderPath;
    if(normFolderPath.back() != pathSeparator)
        normFolderPath = normFolderPath + pathSeparator;
    
	if(isDirectory(normFolderPath))
	{
		DIR* dir = opendir(normFolderPath.c_str());
		if(NULL != dir)
		{
			// 正常打开
			struct dirent* dirEntry = readdir(dir);
			while (dirEntry != NULL)
            {
				if(dirEntry->d_type == DT_DIR)
				{
					// 遍历到目录
					if(strcmp(dirEntry->d_name, ".") != 0 &&
						strcmp(dirEntry->d_name, "..") != 0)
					{
						string newFolderPath = normFolderPath + dirEntry->d_name;
						getFilesInFolderRecursively(newFolderPath, filePathes);
					}
				}
				else if(dirEntry->d_type == DT_REG)
				{
					// 遍历到普通文件
					string newFilePath = normFolderPath + dirEntry->d_name;
					filePathes.push_back(newFilePath);
				}
				else
				{
					// 遍历到其他类型文件
                    string newFilePath = normFolderPath + dirEntry->d_name;
                    cerr<<"警告：遍历到非文件、非文件夹文件：" <<newFilePath<<endl;
				}

				// 读取下一个目录条目
				dirEntry = readdir(dir);
			}
            
            return true;
            
		}else
        {
            cerr<<"错误：打开目录失败!:"<<normFolderPath<<endl;
        }
	}
    else
    {
        cerr<<"错误：要遍历的不是文件夹:"<<normFolderPath<<endl;
    }
    
    return false;
}

bool PlatformSpecificUnixLike::getFilesRelativePathInFolderRecursive(
                                            const std::string& filePath,
                                           std::list<std::string>& filePathes)
{
    // 文件分隔符
    char pathSeparator = PlatformSpecific::getInstance()->getFilePathSeparator();
    
    // 规范目录
    string normFilePath = filePath;
    if(normFilePath.back() != pathSeparator)
        normFilePath = normFilePath + pathSeparator;
    
    // 获得所有子文件
    if(PlatformSpecific::getInstance()->getFilesInFolderRecursively(normFilePath, filePathes))
    {
        // 转换成相对路径
        for(list<string>::iterator it = filePathes.begin();
            it != filePathes.end();
            it++)
        {
            string& path = (*it);
            path.replace(0, normFilePath.length(), "");
        }
        
        return true;
    }
    
    return false;
}

std::string PlatformSpecificUnixLike::getUnixPathFormat(const std::string& path)
{
    return path;
}

std::string PlatformSpecificUnixLike::getWindowsPathFormat(const std::string& path)
{
    // 替换所有的/字符
    int startPos = 0;
    
    string windowsPath = path;
    
    int occurPos = windowsPath.find("/", startPos);
    while(occurPos != string::npos)
    {
        windowsPath.replace(occurPos, 1, "\\");
        startPos += 1;
        occurPos = windowsPath.find("/", startPos);
    }
    
    return windowsPath;
}

std::string PlatformSpecificUnixLike::getParentFolder(const std::string& filePath)
{
    char pathSeparator = getFilePathSeparator();
    string parentFolderPath = filePath;
    

    // 判断最后一个字符是不是 /
    int startPos = parentFolderPath.length() - 1;
    
    if(parentFolderPath.back() == pathSeparator)
    {
        startPos = parentFolderPath.length()-1 -1;
        if(startPos < 0)
            return "";
    }
    
    // 查找下一个/
    int pos = parentFolderPath.rfind(parentFolderPath, startPos);
    if(pos != string::npos)
    {
        // 替换掉之后的字符
        parentFolderPath.replace(pos, string::npos, "");
    }
    return parentFolderPath;
}

std::string PlatformSpecificUnixLike::getLastFileNameInPath(const std::string& filePath)
{
    char pathSeparator = getFilePathSeparator();
    
    int pos = filePath.rfind(pathSeparator);
    if(pos != string::npos)
    {
        int startPos = pos + 1;
        if(startPos > filePath.length()-1)
            return "";
        
        string lastFileName(filePath, startPos);
        return lastFileName;
    }
    
    return filePath;
}

#endif // UNIX_LIKE
