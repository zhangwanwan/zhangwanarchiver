
#ifndef __PLATFORM_SPECIFIC_UNIX_LIKE__
#define __PLATFORM_SPECIFIC_UNIX_LIKE__

#include "PlatformSpecific.h"

#ifdef UNIX_LIKE

class PlatformSpecificUnixLike : public PlatformSpecific
{
public:
	// 文件和目录操作
    bool getFilesInFolderRecursively(const std::string& filePath,
                                     std::list<std::string>& filePathes);
    
    bool getFilesRelativePathInFolderRecursive(const std::string& filePath,
                                               std::list<std::string>& filePathes);
    
    bool isDirectory(const std::string& filePath);
    bool isRegularFile(const std::string& filePath);
    char getFilePathSeparator();
    
    std::string getUnixPathFormat(const std::string& path);
    std::string getWindowsPathFormat(const std::string& path);
    
    std::string getParentFolder(const std::string& filePath);
    std::string getLastFileNameInPath(const std::string& filePath);
};

#endif // UNIX_LIKE

#endif // !__PLATFORM_SPECIFIC_UNIX_LIKE__





