
#include "PlatformSpecificWindows.h"

#ifdef WINDOWS

#include <windows.h>

bool PlatformSpecificWindows::isPathExist(string filePath)
{
	DWORD attrib = GetFileAttributes((LPCTSTR)filePath.c_str());
	if(attrib != INVALID_FILE_ATTRIBUTES)
	{
		return true;
	}
	return false;
}

bool PlatformSpecificWindows::getFilesInFolderRecursively(string& folderPath, list<string>& filePathes)
{
	// 路径是否存在
	if(isPathExist(folderPath.c_str()))
	{
		// 路径是目录
		if(isDirectory(folderPath))
		{
			// 遍历得到所有源代码文件
			string fileFindPattern = folderPath.c_str();
			fileFindPattern += "\\*";
			WIN32_FIND_DATA fdFile;
			HANDLE fileHandle = FindFirstFile(fileFindPattern.c_str(), &fdFile);
			if(fileHandle != INVALID_HANDLE_VALUE)
			{
				do 
				{
					if(strcmp(fdFile.cFileName, ".") != 0 &&
						strcmp(fdFile.cFileName, "..") != 0)
					{
						// 遍历到目录
						if(fdFile.dwFileAttributes &FILE_ATTRIBUTE_DIRECTORY)
						{
							string nextFolder = folderPath + "\\" + fdFile.cFileName;
							getFilesInFolderRecursively(nextFolder, filePathes);
						}
						else
						{
							// 遍历到文件
							string fullFilePath = filePath + "\\" + fdFile.cFileName;
							filePathes.push_back(fullFilePath);
						}
					}
				} while (FindNextFile(fileHandle, &fdFile) != 0);

				FindClose(fileHandle);
			}else
            {
                cerr<<"错误：打开目录失败!:"<<folderPath<<endl;
            }
		}else
		{
            cerr<<"错误：要遍历的不是文件夹:"<<folderPath<<endl;
		}
	}
    
    return false;
}

bool PlatformSpecificWindows::isDirectory(const string& filePath)
{
	DWORD attrib = GetFileAttributes(filePath.c_str());
	if((attrib & FILE_ATTRIBUTE_DIRECTORY) != 0)
	{
		// 是目录
		return true;
	}

	return false;
}

bool PlatformSpecificWindows::isRegularFile(const string& filePath)
{
	if(isPathExist(filePath) && !isDirectory(filePath))
		return true;

	return false;
}

char PlatformSpecificWindows::getFilePathSeparator()
{
	return '\\';
}

string PlatformSpecificWindows::getUnixPathFormat(const std::string& path);
{
    // 替换所有的\字符
    int startPos = 0;
    
    string unixPath = path;
    
    int occurPos = unixPath.find("\\", startPos);
    while(occurPos != string::npos)
    {
        unixPath.replace(occurPos, 1, "/");
        startPos += 1;
        occurPos = unixPath.find("\\", startPos);
    }
    
    return unixPath;
}

std::string PlatformSpecificWindows::getWindowsPathFormat(const std::string& path)
{
    return path;
}

#endif // WINDOWS
