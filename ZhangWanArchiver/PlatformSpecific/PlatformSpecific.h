//
//  PlatformSpecific.h
//  ZhangWanArchiver
//
//  Created by admin on 16-3-16.
//  Copyright (c) 2016年 admin. All rights reserved.
//

#ifndef __ZhangWanArchiver__PlatformSpecific__
#define __ZhangWanArchiver__PlatformSpecific__

#include <stdio.h>
#include <string>
#include <list>
#include <fstream>
#include <cstdint>

class PlatformSpecific
{
public:
    // 文件和目录操作
    virtual bool getFilesInFolderRecursively(const std::string& filePath,
                                             std::list<std::string>& filePathes) = 0;
    virtual bool getFilesRelativePathInFolderRecursive(const std::string& filePath,
                                                       std::list<std::string>& filePathes) = 0;
    
    virtual bool isDirectory(const std::string& filePath) = 0;
    virtual bool isRegularFile(const std::string& filePath) = 0;
    
    virtual char getFilePathSeparator() = 0;
    
    bool getFileLength(const std::string& filePath, long& fileLength);
    
    virtual std::string getUnixPathFormat(const std::string& path) = 0;
    virtual std::string getWindowsPathFormat(const std::string& path) = 0;
    
    virtual std::string getParentFolder(const std::string& filePath) = 0;
    virtual std::string getLastFileNameInPath(const std::string& filePath) = 0;
    
    // 序列化、反序列化
    bool serialize(int32_t value, std::ofstream& stream);
    bool deserialize(int32_t& value, std::ifstream& stream);

private:
	static PlatformSpecific* instance;
public:
	static PlatformSpecific* getInstance();
};


#endif /* defined(__ZhangWanArchiver__PlatformSpecific__) */
