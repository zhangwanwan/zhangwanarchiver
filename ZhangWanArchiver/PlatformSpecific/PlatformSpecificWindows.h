
#ifndef __PLATFORM_SPECIFIC_WINDOWS__
#define __PLATFORM_SPECIFIC_WINDOWS__

#include "PlatformSpecific.h"

#ifdef WINDOWS

class PlatformSpecificWindows: public PlatformSpecific
{
public:
	// 文件和目录操作
	bool getFilesInFolderRecursively(const string& filePath, list<string>& filePathes);
	bool isDirectory(const string& filePath);
	bool isRegularFile(const string& filePath);
	char getFilePathSeparator();

	bool isPathExist(const string filePath);
    
    std::string getUnixPathFormat(const std::string& path);
    std::string getWindowsPathFormat(const std::string& path);
};

#endif // WINDOWS

#endif // !__PLATFORM_SPECIFIC_WINDOWS__
