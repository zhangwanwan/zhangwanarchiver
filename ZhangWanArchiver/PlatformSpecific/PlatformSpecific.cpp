//
//  PlatformSpecific.cpp
//  ZhangWanArchiver
//
//  Created by admin on 16-3-16.
//  Copyright (c) 2016年 admin. All rights reserved.
//

#include "PlatformSpecific.h"

#include <errno.h>

#include <iostream>
#include <fstream>

#include "PlatformSpecificUnixLike.h"
#include "PlatformSpecificWindows.h"

#ifdef UNIX_LIKE

#endif // UNIX_LIKE

using namespace std;

PlatformSpecific* PlatformSpecific::instance = NULL;

PlatformSpecific* PlatformSpecific::getInstance()
{
	if(instance == NULL)
	{
#ifdef UNIX_LIKE
		instance = new PlatformSpecificUnixLike();
#elif WINDOWS
		instance = new PlatformSpecificWindows();
#endif // UNIX_LIKE
	}

	return instance;
}

bool PlatformSpecific::getFileLength(const string& filePath, long& fileLength)
{
	if(isRegularFile(filePath))
	{
		ifstream file;
		file.open(filePath.c_str(), ifstream::in | ifstream::binary);
		if(file.good())
		{
			file.seekg(0, ios_base::end);
			long curPos = file.tellg();
			file.close();
			fileLength = curPos;

			if(file.good())
				return true;
		}
	}
	return false;
}

// 以小端方式，序列化32位整数
bool PlatformSpecific::serialize(int32_t value, ofstream& stream)
{
    stream.put(value);          // 写入低8位
    if(!stream.good())
        return false;
    
    stream.put(value >> 8);     // 写入 8~16位
    if(!stream.good())
        return false;
    
    stream.put(value >> 16);    // ..
    if(!stream.good())
        return false;
    
    stream.put(value >> 24);
    if(!stream.good())
        return false;
    
    return true;
}

// 和序列化写入顺序对应，反序列化int32_t
bool PlatformSpecific::deserialize(int32_t& value, ifstream& stream)
{
    value = 0;
    int32_t tmp = 0;
    
    // 读取低8位
    tmp = stream.get();
    if(!stream.good()) return false;
    value += tmp;
    
    // 读取8-16位
    tmp = stream.get();
    if(!stream.good()) return false;
    value += tmp << 8;
    
    tmp = stream .get();
    if(!stream.good()) return false;
    value += tmp << 16;
    
    tmp = stream.get();
    if(!stream.good()) return false;
    value += tmp << 24;
    
    return true;
}



