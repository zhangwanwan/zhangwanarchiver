//
//  ArchivePacker.cpp
//  ZhangWanArchiver
//
//  Created by admin on 16/3/24.
//  Copyright © 2016年 admin. All rights reserved.
//

#include <iostream>

#include "ArchivePacker.hpp"
#include "PlatformSpecific.h"
#include "ArchiveV1Packer.hpp"

using namespace std;

// 只添加文件索引结构到文件头部
// 记录文件路径和文件大小
// 文件内容偏移，在真正保存到文件的时候填写
bool ArchivePacker::addRelativeFile(const std::string& relativeFilePath)
{
#ifdef UNIX_LIKE
    // unix系统下打包，检查文件名称中是否包含"\"字符
    // unix可以使用"\"作为文件名称，而"\"字符在windows下是路径分隔符
    // 如果有解包功能会出错，所以这里限制限制
    if(relativeFilePath.find("\\") != string::npos)
        return false;
#endif
    
    char pathSeparator = PlatformSpecific::getInstance()->getFilePathSeparator();
    string fileFullPath = rootFilePath + pathSeparator + relativeFilePath;
    string relativeFilePathUnixFormat = PlatformSpecific::getInstance()->getUnixPathFormat(relativeFilePath);
    
    // 文件路径没有被添加过 &&
    // 文件是否存在
    if(fileInArchiveMap.count(relativeFilePathUnixFormat) == 0 &&
       PlatformSpecific::getInstance()->isRegularFile(fileFullPath))
    {
        // 获得文件大小
        long fileLength = 0;
        if(PlatformSpecific::getInstance()->getFileLength(fileFullPath, fileLength))
        {
            // 创建记录
            FileInArchive file(relativeFilePath, 0, fileLength);
            fileInArchiveMap[relativeFilePath] = file;
            return true;
        }
    }
    
    return false;
}

//
bool ArchivePacker::addFileOrFolderToArchive(const std::string folderOrFilePath)
{
    // 检查是文件还是目录
    if(PlatformSpecific::getInstance()->isRegularFile(folderOrFilePath))
    {
        // 得到文件父目录，作为根目录
        string parentFolderPath = PlatformSpecific::getInstance()->getParentFolder(folderOrFilePath);
        string filePath = PlatformSpecific::getInstance()->getLastFileNameInPath(folderOrFilePath);
        
        rootFilePath = parentFolderPath;
        
        // 添加文件
        if(addRelativeFile(filePath))
            return true;
        
        cout<<"错误：添加文件失败："<<folderOrFilePath<<endl;
        return false;
    }
    else if(PlatformSpecific::getInstance()->isDirectory(folderOrFilePath))
    {
        // 文件分隔符
        char pathSeparator = PlatformSpecific::getInstance()->getFilePathSeparator();
        
        // 此目录作为资源包根目录
        rootFilePath = folderOrFilePath;
        
        // 规范目录末尾/
        if(rootFilePath.back() != pathSeparator)
            rootFilePath = rootFilePath + pathSeparator;
        
        // 获得所有子文件
        list<string> filesPath;
        if(PlatformSpecific::getInstance()->getFilesRelativePathInFolderRecursive(folderOrFilePath, filesPath))
        {
            // 添加文件
            for(list<string>::iterator it = filesPath.begin();
                it != filesPath.end();
                it++)
            {
                string& filePath = (*it);
                cout<<"需要添加文件："<<(*it)<<endl;
                
                if(!addRelativeFile(filePath))
                {
                    cout<<"错误：添加文件："<<filePath<<endl;
                    return false;
                }
            }
            
            return true;
            
        }else
        {
            cout<<"错误：递归获得子文件:"<<endl;
        }
    }else
    {
        cerr<<"错误：Archive::addFileOrFolderToArchive 路径不是文件也不是目录:"<<folderOrFilePath<<endl;
    }
    
    return false;
}

void ArchivePacker::debugPrint()
{
    for(FileInArchiveMap::iterator it = fileInArchiveMap.begin();
        it != fileInArchiveMap.end();
        it++)
    {
        FileInArchive& fileInArchive = (*it).second;
        fileInArchive.debugPrint();
    }
}

ArchivePacker* ArchivePacker::createPacker(int version)
{
    if(version == 1)
    {
        ArchivePacker* packer = new ArchiveV1Packer();
        return packer;
    }
    
    return NULL;
}




