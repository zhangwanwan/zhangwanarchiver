//
//  Utilities.cpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "Utilities.hpp"
#include "stdlib.h"

void Utilities::getRandomString(string& randStr, int len)
{
    randStr = "";
    
    for(int i=0;i<len;++i)
    {
        char randChar = 'A' + rand()%26;
        randStr += randChar;
    }
}

char Utilities::getRandomChar()
{
    return 'A' + rand()%26;
}

int Utilities::getRandNumber(int minValue, int maxValue)
{
    if(maxValue == minValue) return minValue;
    int randLength = maxValue - minValue + 1;
    int randValue = rand() % randLength;
    randValue += minValue;
    return randValue;
}