//
//  Utilities.hpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef Utilities_hpp
#define Utilities_hpp

#include <stdio.h>
#include <string>

using namespace std;

class Utilities
{
public:
    static int getRandNumber(int minValue, int maxValue);
    static void getRandomString(string& randStr, int len);
    static char getRandomChar();
};

#endif /* Utilities_hpp */
