//
//  FileSystem.cpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "FileSystem.hpp"
#include "Utilities.hpp"

#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__

#include <fstream>
#include <iostream>
#include <queue>

using namespace std;

char FileSytem::getPathSeparator()
{
    return '/';
}

bool FileSytem::getChildFolderName(string& parentFolderPath, list<string>& childFolderName)
{
    // 打开当前目录，随机选择一个子目录
    DIR* dir = opendir(parentFolderPath.c_str());
    bool result = false;
    if(dir != NULL)
    {
        result = getChildFolderName(dir, childFolderName);
        closedir(dir);
    }
    return result;
}

bool FileSytem::getChildFolderName(DIR* parentFolder, list<string>&  childFolderName)
{
    if(parentFolder != NULL)
    {
        // 正常打开
        struct dirent* dirEntry = readdir(parentFolder);
        while (dirEntry != NULL) {
            if(dirEntry->d_type == DT_DIR)
            {
                // 遍历到目录
                if(strcmp(dirEntry->d_name, ".") != 0 &&
                   strcmp(dirEntry->d_name, "..") != 0)
                {
                    childFolderName.push_back(dirEntry->d_name);
                }
            }
            
            // 读取下一个目录条目
            dirEntry = readdir(parentFolder);
        }
        
        return true;
    }
    
    return false;
}

bool FileSytem::isFolderExist(string& path)
{
    DIR *dp;
    if ((dp = opendir(path.c_str())) == NULL)
    {
        return false;
    }
    
    closedir(dp);
    return true;
}

bool FileSytem::isFileExist(string& path)
{
    if(access(path.c_str(), F_OK) == 0)
        return true;
    return false;
}

// 在指定深度，创建随机文件
bool FileSytem::createRandomFileAtDepth(string& rootPath,
                                        int depth,
                                        int fileSize,
                                        string& outFilePath)
{
    string randomFolder;
    if(getRandomFolderByDepth(rootPath, depth-1, randomFolder))
    {
        // 创建随机文件
        do
        {
            // 生成随机名称
            string randomName;
            Utilities::getRandomString(randomName, 10);
            randomName += ".file";
            
            // 检查是否存在
            string randomNamePath = randomFolder + FileSytem::getPathSeparator() + randomName;
            if(!FileSytem::isFileExist(randomNamePath) && !FileSytem::isFolderExist(randomNamePath))
            {
                // 创建文件
                ofstream file;
                file.open(randomNamePath.c_str(), ofstream::out | ofstream::binary);
                if(file.good())
                {
                    // 写入指定大小
                    for(int i=0; i<fileSize; i++)
                    {
                        file.put(Utilities::getRandomChar());
                        
                        if(!file.good())
                        {
                            file.close();
                            return false;
                        }
                    }
                    file.close();
                    outFilePath = randomNamePath;
                    return true;
                }
            }
        }
        while (true);
    }
    
    return false;
}

bool FileSytem::createRandomFolderAtDepth(string& rootPath, int depth)
{
    string parentFolderPath = "";
    
    // 获得上一层目录
    if(depth == 0)
        parentFolderPath = rootPath;
    else
    {
        if(!getRandomFolderByDepth(rootPath, depth-1, parentFolderPath))
        {
            cout<<"错误createRandomFolderAtDepth：getRandomFolderByDepth"<<endl;
            return false;
        }
    }
    
    // 在此目录下创建文件夹
    if(!createRandomFolderInFolder(parentFolderPath))
    {
        cout<<"错误createRandomFolderAtDepth：createRandomFolderInFolder"<<endl;
        return false;
    }
    
    
    return true;
}

bool FileSytem::createRandomFolderInFolder(string& folderPath)
{
    int result;
    do
    {
        string randomName;
        Utilities::getRandomString(randomName, 10);
        
        // 检查是否存在
        string randomFolderPath = folderPath + FileSytem::getPathSeparator() + randomName;
        
        // 尝试创建目录
        result = mkdir(randomFolderPath.c_str(), 0777);
        if(result == 0)
        {
            cout<<"创建目录成功："<<randomFolderPath<<endl;
            return true;
        }else
        {
            if(errno != EEXIST)
            {
                char * mesg = strerror(errno);
                printf("错误createRandomFolderInFolder: %s\n",mesg);
                return false;
            }
        }
    }
    while (errno == EEXIST);
    
    return false;
}

// 得到目录下随机子目录
bool FileSytem::getRandomFolderInFolder(string& folderPath, string& randomFolderName)
{
    list<string> childFolderNameList;
    
    if(FileSytem::getChildFolderName(folderPath, childFolderNameList))
    {
        // 随机选择一个
        int childFolderCount = childFolderNameList.size();
        int randomIndex = Utilities::getRandNumber(0, childFolderCount-1);
        // 得到randomIndex处的元素
        list<string>::iterator nthIterator = childFolderNameList.begin();
        advance(nthIterator, randomIndex);
        randomFolderName = *nthIterator;
        return true;
    }else{
        cout<<"错误getRandomFolderInFolder "<<endl;
    }
    
    return false;
}

struct PathAndDepth
{
    string path;
    int curDepth;
};

// 广度遍历，得到所有深度是depth的目录
bool FileSytem::getAllFolderByDepth(string& rootPath, int depth, list<string>& folderPathes)
{
    queue<PathAndDepth> pathAndDepthQueue;
    PathAndDepth root = {rootPath, -1};
    pathAndDepthQueue.push(root);
    
    while(!pathAndDepthQueue.empty())
    {
        PathAndDepth pathAndDepth = pathAndDepthQueue.front();
        pathAndDepthQueue.pop();
        
        // 如果是要求的深度
        if(pathAndDepth.curDepth == depth)
        {
            folderPathes.push_back(pathAndDepth.path);
        }else if(pathAndDepth.curDepth < depth)
        {
            // 添加此目录的所有子目录，到queue
            // 得到所有子目录名称
            list<string> childFolderNames;
            FileSytem::getChildFolderName(pathAndDepth.path, childFolderNames);
            // 填写PathAndDepth结构，添加到queue
            for(list<string>::iterator it = childFolderNames.begin();
                it != childFolderNames.end();
                it++)
            {
                PathAndDepth newPathAndDepth;
                newPathAndDepth.path = pathAndDepth.path + FileSytem::getPathSeparator() + (*it);
                newPathAndDepth.curDepth = pathAndDepth.curDepth + 1;
                pathAndDepthQueue.push(newPathAndDepth);
            }
            
        }else
        {
            cout<<"错误getAllFolderByDepth：出现不可能的情况！"<<endl;
            return false;
        }
    }
    
    return true;
}

// 得到某一深度下的一随机目录
bool FileSytem::getRandomFolderByDepth(string& rootPath, int depth, string& randomFolderPath)
{
    // 得到所有深度是depth的目录
    list<string> allDepthFolderPathes;
    if(getAllFolderByDepth(rootPath, depth, allDepthFolderPathes))
    {
        // 从中随机选择一个
        int count = allDepthFolderPathes.size();
        if(count > 0)
        {
            int randomIndex = Utilities::getRandNumber(0, count-1);
            list<string>::iterator it = allDepthFolderPathes.begin();
            advance(it, randomIndex);
            randomFolderPath = *it;
            return true;
        }else
        {
            cout<<"错误getRandomFolderByDepth: if(count > 0)"<<endl;
        }
    }else
    {
        cout<<"错误getRandomFolderByDepth: if(getAllFolderByDepth(depth, allDepthFolderPathes))"<<endl;
    }
    
    return false;
}

#endif
