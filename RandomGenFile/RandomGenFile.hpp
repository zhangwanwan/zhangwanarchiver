//
//  RandomGenFile.hpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef RandomGenFile_hpp
#define RandomGenFile_hpp

#include <stdio.h>
#include <string>
#include <map>
#include <list>

using namespace std;

class FileInfo
{
public:
    int depth;
    int size;
    string path;
};

// map<depth, folder count>
typedef map<int, int> FolderInfoMap;

class RandomGenFile
{
private:
    
    // 文件大小范围
    int minFileSize;
    int maxFileFize;
    // 文件个数范围
    int minFileNum;
    int maxFileNum;
    // 文件深度层次范围
    int minFileDepth;
    int maxFileDepth;
    
    // 文件夹个数范围
    int minFolderNum;
    int maxFolderNum;
    // 文件夹深度范围
    int minFolderDepth;
    int maxFolderDepth;
    
    // 真正随机到的文件个数
    int trueFileNum;
    // 真正生成的文件总大小
    int totalFileSize;
    
    // 真正生成的文件夹个数
    int trueFolderNum;
    
    // 要生成的文件信息
    list<FileInfo> files;
    // 要生成的文件夹信息
    FolderInfoMap folderInfoMap;
    
    // 要生成的根目录
    string rootFolderPath;
    
private:
    
    // 输入
    void getParameterRangeFromUser(const string& tips, int& minValuel, int& maxValue, bool positive);
    bool getRootFolderPathFromUser();
    
    int getTrueMaxFileDepth();
    
    // 生成过程
    void randomFileInfo();
    bool randomFolderInfo();
    bool genFolderAndFile();
    
    bool saveToConfigFile(string& configPath);
    
    //
    void getNRandomFilePath(int n, list<string>& outFilePathes);
    
public:
    void run(int argc, const char * argv[]);
};


#endif /* RandomGenFile_hpp */
