//
//  FileSystem.hpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#ifndef FileSystem_hpp
#define FileSystem_hpp

#include <stdio.h>
#include <string>
#include <list>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>

using namespace std;

class FileSytem
{
public:
    // 文件系统相关
    static bool getChildFolderName(string& parentFolderPath, list<string>& childFolderName);
    static bool getChildFolderName(DIR* parentFolder, list<string>& childFolderName);
    static char getPathSeparator();
    
    // 关于目录
    static bool getRandomFolderInFolder(string& folderPath, string& randomFolderName);
    static bool createRandomFolderInFolder(string& folderPath);
    
    static bool getAllFolderByDepth(string& roothPath, int depth, list<string>& folderPathes);
    static bool getRandomFolderByDepth(string& rootPath, int depth, string& randomFolderName);
    static bool createRandomFolderAtDepth(string& rootPath, int depth);
    
    // 关于文件
    static bool createRandomFileAtDepth(string& rootPath, int depth, int fileSize, string& outFilePath);
    
    // 检查目录是否存在
    static bool isFolderExist(string& path);
    //检查文件(所有类型)是否存在
    static bool isFileExist(string& path);
};

#endif /* FileSystem_hpp */
