//
//  RandomGenFile.cpp
//  RandomGenFile
//
//  Created by admin on 16/3/21.
//  Copyright © 2016年 admin. All rights reserved.
//

#include "RandomGenFile.hpp"
#include "Utilities.hpp"

#include <iostream>
#include <list>
#include <fstream>

#include "time.h"

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <vector>
#include <queue>

#include "FileSystem.hpp"

using namespace std;

void RandomGenFile::getParameterRangeFromUser(const string& tips, int& minValuel, int& maxValue, bool positive)
{
    char c = '~';
    bool isValid = false;
    
    do
    {
        cout<<tips<<endl;
        cin>>minValuel;
        maxValue = minValuel;
        
        // 检查参数正确
        isValid = false;
        if(positive)
        {
            if(c == '~' && maxFileFize >= minFileSize && minFileSize >= 0)
                isValid = true;
        }else
        {
            if(c == '~' && maxFileFize >= minFileSize)
                isValid = true;
        }
    }while(!isValid);
}

int RandomGenFile::getTrueMaxFileDepth()
{
    int maxDepth = 0;
    for(list<FileInfo>::iterator it = files.begin();
        it != files.end();
        it++)
    {
        FileInfo& fileInfo = *it;
        if(fileInfo.depth > maxDepth)
            maxDepth = fileInfo.depth;
    }
    
    return maxDepth;
}

void RandomGenFile::randomFileInfo()
{
    // 文件个数
    int fileCount =  Utilities::getRandNumber(minFileNum, maxFileNum);
    int totalSize = 0;
    minFileDepth = 0;
    for(int i=0; i<fileCount; i++)
    {
        // 文件大小
        int fileSize = Utilities::getRandNumber(minFileSize, maxFileFize);
        int fileDepth = Utilities::getRandNumber(minFileDepth, maxFileDepth);
        
        FileInfo newFile;
        newFile.depth = fileDepth;
        newFile.size = fileSize;
        files.push_back(newFile);
        
        totalSize += fileSize;
    }
    
    trueFileNum = fileCount;
    totalFileSize = totalSize;
    
    //
    cout<<"文件个数："<<fileCount<<
    " 最大深度："<<getTrueMaxFileDepth()<<
    " 总大小："<<totalSize<<endl;
}

bool RandomGenFile::randomFolderInfo()
{
    // 获得文件最大深度
    int trueMaxFileDepth = getTrueMaxFileDepth();
    
    // 至少要创建 trueMaxFileDepth个文件夹
    int mustHaveFolderNum = trueMaxFileDepth;
    if(mustHaveFolderNum > 0)
    {
        for(int i=0; i<mustHaveFolderNum; i++)
        {
            folderInfoMap[i] = 1;
        }
    }
    
    // 创建其他文件夹
    int folderNumToCreate = Utilities::getRandNumber(minFolderNum, maxFolderNum);
    int extraFolderToCreate = folderNumToCreate - mustHaveFolderNum;
    if(extraFolderToCreate < 0) extraFolderToCreate = 0;
    
    // 限制目录的深度到文件深度内
    minFolderDepth = 0;
    if(minFolderDepth > trueMaxFileDepth - 1)
        minFolderDepth = trueMaxFileDepth - 1;
    
    if(maxFolderDepth > trueMaxFileDepth - 1)
        maxFolderDepth = trueMaxFileDepth - 1;
    
    if(extraFolderToCreate > 0)
    {
        for(int i=0; i<extraFolderToCreate; i++)
        {
            int folderDepth = Utilities::getRandNumber(minFolderDepth, maxFolderDepth);
            folderInfoMap[folderDepth]++;
        }
    }
    
    // 检查正确性
    for(int i=0; i<trueMaxFileDepth; i++)
    {
        if(folderInfoMap[i] <= 0)
            return false;
    }
    
    trueFolderNum = folderNumToCreate;
    cout<<"文件夹个数："<<folderNumToCreate<<endl;
    return true;
}

bool RandomGenFile::genFolderAndFile()
{
    // 先生成文件夹
    int trueMaxFileDepth = getTrueMaxFileDepth();
    for(int depth=0; depth<trueMaxFileDepth; depth++)
    {
        // 得到某一深度下文件夹个数
        int folderCountAtDepth = folderInfoMap[depth];
        if(folderCountAtDepth <= 0)
        {
            cout<<"genFolderAndFile错误：folderCountAtDepth <= 0"<<endl;
            return false;
        }
        
        for(int i=0; i<folderCountAtDepth; i++)
        {
            // 随机创建深度为depth的文件夹
            if(!FileSytem::createRandomFolderAtDepth(rootFolderPath, depth))
            {
                cout<<"genFolderAndFile错误：!createRandomFolderAtDepth(depth)"<<endl;
                return false;
            }
        }
    }
    
    cout<<"生成文件夹成功！"<<endl;
    
    // 生成文件
    for(list<FileInfo>::iterator it = files.begin();
        it != files.end();
        it ++
        )
    {
        FileInfo& fileInfo = *it;
        string createdFilePath;
        if(!FileSytem::createRandomFileAtDepth(rootFolderPath,
                                               fileInfo.depth,
                                               fileInfo.size,
                                               createdFilePath))
        {
            cout<<"genFolderAndFile错误：createRandomFileAtDepth(fileInfo.depth, fileInfo.size)"<<endl;
            return false;
        }
        
        fileInfo.path = createdFilePath;
    }
    
    cout<<"生成文件成功！"<<endl;
    
    return true;
}

bool RandomGenFile::getRootFolderPathFromUser()
{
    cout<<"输入输出目录："<<endl;
    cin>>rootFolderPath;
    
    // 检查是否存在
    if(FileSytem::isFolderExist(rootFolderPath))
        return true;
    
    return false;
}

void RandomGenFile::getNRandomFilePath(int n, list<string>& outFilePathes)
{
    if(n > files.size())
        n = files.size();
    
    int remaining = files.size();

    for(list<FileInfo>::iterator it = files.begin();
        it != files.end();
        it++)
    {
        if(rand() % remaining < n)
        {
            n--;
            FileInfo& info = *it;
            outFilePathes.push_back(info.path);
        }
        
        remaining--;
    }
}

bool RandomGenFile::saveToConfigFile(string& configPath)
{
    ofstream configFile;
    configFile.open(configPath, ofstream::out);
    if(configFile.good())
    {
        configFile<<"#文件大小范围:"<<minFileSize<<"-"<<maxFileFize<<endl;
        configFile<<"#文件个数范围:"<<minFileNum<<"-"<<maxFileNum<<endl;
        configFile<<"#文件深度范围:"<<minFileDepth<<"-"<<maxFileDepth<<endl;
        configFile<<"#文件夹个数范围:"<<minFolderNum<<"-"<<maxFolderNum<<endl;
        configFile<<"#文件夹深度范围:"<<minFolderDepth<<"-"<<maxFolderDepth<<endl;
        
        configFile<<"#文件总个数:"<<trueFileNum<<endl;
        configFile<<"#文件最大深度:"<<maxFileDepth<<endl;
        configFile<<"#文件总大小："<<totalFileSize<<endl;
        
        configFile<<"#总文件夹个数:"<<trueFolderNum<<endl;
        
        // 打印出生成的文件路名
        for(list<FileInfo>::iterator it = files.begin();
            it != files.end();
            it++)
        {
            FileInfo& info = *it;
            configFile<<info.path<<endl;
        }
        
        configFile.close();
    }else
    {
        cout<<"打开配置文件："<<configPath<<"失败！"<<endl;
        return false;
    }
    
    return true;
}

void RandomGenFile::run(int argc, const char * argv[])
{
    srand (time(NULL));
    
    // 输入参数
    getParameterRangeFromUser("输入文件大小范围：", minFileSize, maxFileFize, true);
    getParameterRangeFromUser("输入文件个数范围：", minFileNum, maxFileNum, true);
    getParameterRangeFromUser("输入文件深度范围：", minFileDepth, maxFileDepth, true);
    getParameterRangeFromUser("输入文件夹个数范围：", minFolderNum, maxFolderNum, true);
    getParameterRangeFromUser("输入文件夹深度范围：", minFolderDepth, maxFolderDepth, true);
    
    bool rootPathOk = false;
    rootPathOk = getRootFolderPathFromUser();
    if(!rootPathOk)
    {
        cout<<"输入的文件夹不存在："<<rootFolderPath<<endl;
        return;
    }
    
    // 根据参数计算要生成的文件信息
    randomFileInfo();
    
    // 根据参数计算要生成的目录信息
    if(!randomFolderInfo())
    {
        cout<<"生成文件夹信息错误！"<<endl;
        return;
    }
    
    // 生成目录和文件
    if(!genFolderAndFile())
        cout<<"生成目录和文件错误！"<<endl;
    
//    // 生成要读取的随机文件列表
//    cout<<"输入随机选择的文件个数："<<endl;
//    int selectedFileCount = 10;
//    cin>>selectedFileCount;
//    list<string> randomFilePathes;
//    getNRandomFilePath(selectedFileCount, randomFilePathes);
    
    // 保存
    // 程序运行参数
    // 随机文件列表到文件
    
    // 配置文件路径
    string configFileName = "config.txt";
    string configFilePath = rootFolderPath + FileSytem::getPathSeparator() + configFileName;
    
    if(saveToConfigFile(configFilePath))
    {
        cout<<"写入配置文件成功！"<<endl;
    }else
    {
        cout<<"写入配置文件失败！"<<endl;
    }
}
